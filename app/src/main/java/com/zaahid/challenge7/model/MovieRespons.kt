package com.zaahid.challenge7.model

import com.google.gson.annotations.SerializedName
import com.zaahid.challenge7.model.Hasil

data class MovieRespons (
    @SerializedName("page")
    val page : Int?,
    @SerializedName("results")
    val results : List<Hasil>,
    @SerializedName("totalPages")
    val totalPages : Int?,
    @SerializedName("totalResult")
    val totalResult : Int?
)