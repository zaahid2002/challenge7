package com.zaahid.challenge7.data.local.database.datasource

import android.app.Application
import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import com.google.common.truth.Truth.assertThat
import com.zaahid.challenge7.data.local.AppDataBase
import com.zaahid.challenge7.data.local.database.dao.UserDao
import com.zaahid.challenge7.data.local.database.entity.UserEntity
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.After

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(manifest = Config.NONE)
class UserDataSourceImplTest {
    private lateinit var userDataSource: UserDataSourceImpl
    private lateinit var database : AppDataBase
    private lateinit var dao : UserDao
    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()

        database = Room.inMemoryDatabaseBuilder(context,AppDataBase::class.java).build()
        dao = database.userDao()
        userDataSource = UserDataSourceImpl(database.userDao())

    }

    @Test
    fun getUserByusername() {
        runBlocking {
            val entity = UserEntity(1,"zaahid","aldnfl","123","aglna")
            dao.insertUser(entity)
        }
        val response = runBlocking {
            dao.getUserByusername("zaahid")
        }
        val result = runBlocking {
            userDataSource.getUserByusername("zaahid")
        }
        assertThat(response ==result).isFalse()
    }
    @After
    fun closedb(){
        database.close()
    }
}