package com.zaahid.challenge7.data.network.datasource

import android.util.Log
import com.google.common.truth.Truth
import com.zaahid.challenge6.data.network.service.TMDBApiService
import com.zaahid.challenge7.BuildConfig
import com.zaahid.challenge7.model.MovieRespons
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.any

const val TAG = "test"
class MovieDataSourceImplTest {
    private lateinit var service: TMDBApiService
    private lateinit var movieDataSourceImpl: MovieDataSourceImpl
    @Before
    fun setUp() {
        service = mockk()
        movieDataSourceImpl = MovieDataSourceImpl(service)
    }

    @Test
    fun searchMovie() {
        // Mocking (GIVEN)
        val response =mockk<MovieRespons>()
        every {
            runBlocking {
                service.searchMovie(BuildConfig.API_KEY,"avenger","en",1)
            }
        }returns response

//        system under test(when)
        val result = runBlocking {
            movieDataSourceImpl.searchMovie("avenger","en",1)
        }
        verify {
            runBlocking { service.searchMovie(BuildConfig.API_KEY,"avenger","en",1) }
        }
        Truth.assertThat(result).isEqualTo(response)
    }
    @Test
    fun searchMovieNegate(){
        // Mocking (GIVEN)
        val response =mockk<MovieRespons>()
        val response1 = mockk<MovieRespons>()
        every {
            runBlocking {
                service.searchMovie(BuildConfig.API_KEY,"avenger","en",1)
            }
        }returns response
        every {
            runBlocking {
                service.searchMovie(BuildConfig.API_KEY,"avenger","en",2)
            }
        }returns response1

//        system under test(when)
        val result = runBlocking {
            movieDataSourceImpl.searchMovie("avenger","en",2)
        }
//        verify {
//            runBlocking { service.searchMovie(BuildConfig.API_KEY,"avenger","en",1) }
//        }
        Truth.assertThat(result).isEqualTo(response1)
    }

    @Test
    fun popularMovie() {
        // Mocking (GIVEN)
        val response =mockk<MovieRespons>()
        every {
            runBlocking {
                service.moviePopular(any(),any(),any())
            }
        }returns response

//        system under test(when)
        val result = runBlocking {
            movieDataSourceImpl.popularMovie(any(), any())
        }
        verify {
            runBlocking { service.moviePopular(any(),any(),any()) }
        }
        Truth.assertThat(result == response).isTrue()
    }

    @Test
    fun popularMovieNegate() {
        // Mocking (GIVEN)
        val response =mockk<MovieRespons>()
        val response1 = mockk<MovieRespons>()
        every {
            runBlocking {
                service.moviePopular(any(),any(),1)
            }
        }returns response
        every {
            runBlocking {
                service.moviePopular(any(),any(),2)
            }
        }returns response1

//        system under test(when)
        val result =runBlocking {
                movieDataSourceImpl.popularMovie(any(),2)
            }

//        verify {
//            runBlocking { service.moviePopular(any(),any(),2) }
//        }

        Truth.assertThat(result).isNotEqualTo(response)
    }

}